import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-conhecimento',
  templateUrl: './conhecimento.component.html',
  styleUrls: ['./conhecimento.component.scss']
})
export class ConhecimentoComponent implements OnInit {

  conhecimentos: string[];

  constructor() {
    this.conhecimentos = [
      "GITHUB",
      "BOOTSTRAP",
      "MATERIALIZE",
      "BULMA",
      "WEBPACK",
      "REACTJS",
      "ANGULAR",
      "NODEJS",
      "EXPRESS JS",
      "SPRINGBOOT",
      "JPA",
      "HIBERNATE",
      "JSP",
      "LOMBOK",
      "JWT",
      "OAUTH",
      "SQLITE",
      "MYSQL",
      "POSTGRESQL",
      "ORACLE",
      "PL/SQL"
    ]
  }

  ngOnInit(): void {
  }

}
