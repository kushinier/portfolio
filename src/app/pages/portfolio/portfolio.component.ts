import { Component, OnInit } from '@angular/core';
import { Portfolio } from './Portolio';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  projetos: Portfolio[];

  constructor() {
    this.projetos = [
      {title: "portifolio", subtitle: "angular/bootstrap/express", path: "assets/projetos/angular-portifolio.png", link: "https://github.com/kushinier/Portifolio"}
    ]
  }

  ngOnInit(): void {
  }

}
