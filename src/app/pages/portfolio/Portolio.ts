export class Portfolio {
    title: string;
    subtitle: string;
    path: string;
    link: string;
}